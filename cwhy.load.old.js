// Create a wrapper for unahledle prose rejection
const Promise = require("bluebird")

// This extends books.parse.validate.js by adding in the bulk write 
// of documents to the mongo db.
// Bulk writes are necessary for performance as mongo will handle 
// the buffering and parallelism of the writes.

const chalk = require('chalk')
const assert = require('assert')

// prepare for mongo connection
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'test'
const client = new MongoClient(url, { useUnifiedTopology: true })

// set up schema and validator
const Ajv = require("ajv").default
const ajv = new Ajv({}) // options can be passed, e.g. {allErrors: true}
const cwhy = require("./cwhy.schema.js")
const { resolve, reject } = require("bluebird")
const validate = ajv.compile(cwhy.schema)

function start() {
  startTime = new Date()
}

function end() {
  endTime = new Date()
  var timeDiff = endTime - startTime //in ms
  timeDiff /= 1000

  var seconds = Math.round(timeDiff)
  console.log(seconds + "s")
}

start()
client.connect( async (err, client) => {
  
  assert.strictEqual(null, err)
  console.log(chalk.blue('Connected successfully to server'))

  const db = client.db(dbName)
  console.log(chalk.blue(`Switched to ${db.databaseName} database`))
  const cwhy = db.collection('coronawhy')
  let bulk = cwhy.initializeUnorderedBulkOp()


  let writes = 0
  let rejected = 0
  let accepted = 0
  const cursor = db.collection('staghing').find({})

  cursor.on("data", async (data) => {
    const valid = validate(data)
    if (valid)  {
      ++accepted
      if (writes >= 100000) {
      //   await new Promise( async (resolve, reject) => {
      //     try {
      //       const res = await bulk.execute()
      //       return resolve(res)
      //     } 
      //     catch (err) {
      //       return resolve()
      //     }
      //   })
        // let res = await bulk.execute()
        bulk.execute()
        .then(res => {return res}
        )
        .catch(err => {
          console.log(chalk.black.bgYellow(err))
        })
        bulk = cwhy.initializeUnorderedBulkOp()
        // bulk.initializeUnorderedBulkOp()
        writes = 0
      }
      bulk.insert( data )
      ++writes
    } else {
      ++rejected
    }
  })
  cursor.on("end", async () => {
    console.log(chalk.blueBright(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`))
    let res = await bulk.execute().catch(err => {
      console.log(chalk.black.bgYellow(err))
      return {nInserted: 0}
    })
    end()
    client.close()
    console.log(chalk.blue('Connection closed'))
  })

  //     // validate streamed JSON object value
  //     const valid = validate(value)
  //     if (valid)  {
        
  //       if (writes>=100000) {
  //         // console.log("bulkwrite")
  //         // end()
  //         bulk.execute()
  //           .then(res => {
  //             console.log(`inserted: ${res.nInserted}`)
  //             cummWrites = cummWrites + writes
  //             console.log(`writes: ${cummWrites}`)
  //             end()
  //           })
  //           .catch(err => {
  //             cummWrites = cummWrites + writes
  //             console.log(`writes: ${cummWrites}`)
  //             end()
  //             // console.log(chalk.black.bgYellow(err))
  //           })
  //         // bulk.initializeUnorderedBulkOp()
  //         writes = 0
  //         // if (cummWrites=1000000) {
  //         //   end()
  //         //   console.log("1 million") 
  //         // }  
  //       }
        
  //       bulk.insert( value )
  //       ++writes
  //       return value  
  //     } else {
  //       // console.log("fail")
  //       ++rejected
  //       // console.log(value._id)
  //       // console.log(validate.errors)
  //       return null
  //     }
  //   }
  // ])

  // let rejected = 0
  // let accepted = 0
  // let writes = 0
  // let cummWrites = 0

  // pipeline.on('data', () => ++accepted)
  // pipeline.on('end', async () => {
  //   console.log(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`)
  //   let res = await bulk.execute().catch(err => {
  //     console.log(chalk.black.bgYellow(err))
  //     return {nInserted: 0}
  //   })
  //   end()
  //   console.log(`inserted: ${res.nInserted}`)
  //   client.close()
  //   console.log(chalk.blue('Connection closed'))
  // })
    
})