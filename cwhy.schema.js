// This is the schema that the JSON schema validator will use. 
// It's based on the $jsonSchema defined in the validation rules for the books collection in the bookdb database.
// I've commented out / replaced items below to convert from Mongo schema validator to JSON schema validator

const schema =  
    {
      // bsonType: 'object',
      type: 'object',
      required: [
        'paper_id',
        'language',
        "section",
        "sentence",
        "lemma",
        'UMLS',
        "GGP",
        "SO",
        "TAXON",
        "CHEBI",
        "GO",
        "CL",
        "DNA",
        "CELL_TYPE",
        "CELL_LINE",
        "RNA",
        "PROTEIN",
        "DISEASE",
        "CHEMICAL",
        "CANCER",
        "ORGAN",
        "TISSUE",
        "ORGANISM",
        "CELL",
        "AMINO_ACID",
        "GENE_OR_GENE_PRODUCT",
        "SIMPLE_CHEMICAL",
        "ANATOMICAL_SYSTEM",
        "IMMATERIAL_ANATOMICAL_ENTITY",
        "MULTI-TISSUE_STRUCTURE",
        "DEVELOPING_ANATOMICAL_STRUCTURE",
        "ORGANISM_SUBDIVISION",
        "CELLULAR_COMPONENT",
        "PATHOLOGICAL_FORMATION",
        "ORGANISM_SUBSTANCE",
        "sentence_id"
      ],
      properties: {
        paper_id: {
          type: 'integer', // instead of BSON `int`
          description: 'must be an integer and is required'
        },
        section: {
          type: 'string',
          description: 'must be a string and is required'
        },
        sentence: {
          type: 'string',
          description: 'must be a string and is required'
        },
        sentence_id: {
          type: 'string',
          description: 'must be a string and is required'
        },
  
        language: {
          'enum': [
            'en',
            // 'fr',
            'cn'
          ],
          description: 'can only be one of the enum values and is required'
        }
      }
    }
// }
module.exports.schema = schema
// {
//   $jsonSchema: {
//     bsonType: 'object',
//     required: [
//       'paper_id',
//       'language',
//       'section',
//       'sentence',
//       'lemma',
//       'UMLS',
//       'GGP',
//       'SO',
//       'TAXON',
//       'CHEBI',
//       'GO',
//       'CL',
//       'DNA',
//       'CELL_TYPE',
//       'CELL_LINE',
//       'RNA',
//       'PROTEIN',
//       'DISEASE',
//       'CHEMICAL',
//       'CANCER',
//       'ORGAN',
//       'TISSUE',
//       'ORGANISM',
//       'CELL',
//       'AMINO_ACID',
//       'GENE_OR_GENE_PRODUCT',
//       'SIMPLE_CHEMICAL',
//       'ANATOMICAL_SYSTEM',
//       'IMMATERIAL_ANATOMICAL_ENTITY',
//       'MULTI-TISSUE_STRUCTURE',
//       'DEVELOPING_ANATOMICAL_STRUCTURE',
//       'ORGANISM_SUBDIVISION',
//       'CELLULAR_COMPONENT',
//       'PATHOLOGICAL_FORMATION',
//       'ORGANISM_SUBSTANCE',
//       'sentence_id'
//     ],
//     properties: {
//       paper_id: {
//         type: 'string',
//         description: 'must be a string and is required'
//       },
//       section: {
//         type: 'string',
//         description: 'must be a string and is required'
//       },
//       sentence: {
//         type: 'string',
//         description: 'must be a string and is required'
//       },
//       sentence_id: {
//         type: 'string',
//         description: 'must be a string and is required'
//       },
//       language: {
//         'enum': [
//           'en',
//           'cn'
//         ],
//         description: 'can only be one of the enum values and is required'
//       }
//     }
//   }
// }