8.7 GB file ~7.5 Million docs

~35 mins via mongo-compass import
~10 mins via mongoimport `mongoimport mongodb://root:access@localhost:27017 --authenticationDatabase admin ./v6_text.json -d test -c coronawhy --drop --jsonArray`
2021-02-25T19:45:24.318+1100
2021-02-25T19:55:23.380+1100

https://stackoverflow.com/questions/21246752/how-to-view-huge-txt-files-in-linux
https://www.cyberciti.biz/faq/linux-command-to-find-the-system-configuration-and-hardware-information/
https://www.kaggle.com/skylord/coronawhy?select=v6_text.json  

export NODE_OPTIONS="--max-old-space-size=8192" #increase to 8gb
# stats

## i5 
Start: Fri Feb 26 2021 17:23:22 GMT+1100 (Australian Eastern Daylight Time)
End: Fri Feb 26 2021 17:32:28 GMT+1100 (Australian Eastern Daylight Time) duration 9 mins 

Start: Fri Feb 26 2021 17:32:28 GMT+1100 (Australian Eastern Daylight Time)
End: Fri Feb 26 2021 17:44:54 GMT+1100 (Australian Eastern Daylight Time) duration 12 mins 

## M1
Start: Fri Feb 26 2021 16:32:46 GMT+1100 (Australian Eastern Daylight Time)
End: Fri Feb 26 2021 16:42:30 GMT+1100 (Australian Eastern Daylight Time) duration 10 mins 

Start: Fri Feb 26 2021 16:42:30 GMT+1100 (Australian Eastern Daylight Time)
End: Fri Feb 26 2021 17:11:16 GMT+1100 (Australian Eastern Daylight Time) duration 29 mins 

## i7
Start: Fri Feb 26 2021 18:34:49 GMT+1100 (Australian Eastern Daylight Time)
End: Fri Feb 26 2021 18:48:00 GMT+1100 (Australian Eastern Daylight Time) duration 13 mins 

Start: Fri Feb 26 2021 18:48:00 GMT+1100 (Australian Eastern Daylight Time)
End: Fri Feb 26 2021 19:06:31 GMT+1100 (Australian Eastern Daylight Time) duration 19 mins 

# dtamgrstr

Data Migration Starter. For those big beautiful JSON files in your life that want to immigrate to MongoDB land.

## Background

## Constraints


## Docker

## CLI quick start

`mongoimport mongodb://root:access@localhost:27017 --authenticationDatabase admin ./books.json -d bookdb -c books --drop`

`db.collection.deleteMany({})`


## Mongo Schema Validation
https://docs.mongodb.com/manual/core/schema-validation/   
https://www.mongodb.com/blog/post/document-validation-part-1-adding-just-the-right-amount-of-control-over-your-documents   
`mongoimport mongodb://root:access@localhost:27017 --authenticationDatabase admin ./v6_text.json -d test -c coronawhy --drop --jsonArray`

https://stackoverflow.com/questions/21246752/how-to-view-huge-txt-files-in-linux
https://www.cyberciti.biz/faq/linux-command-to-find-the-system-configuration-and-hardware-information/
https://www.kaggle.com/skylord/coronawhy?select=v6_text.json  

## Tools

### [Stream JSON](https://www.npmjs.com/package/stream-json)

The foillowing may be handy:
[Verifier](https://github.com/uhop/stream-json/wiki/Verifier): Only purpose is to raise an error with an exact location (offset, line, position) when its input is not a valid JSON. Use it to troubleshoot when Parser fails to parse.  

[Stringer](https://github.com/uhop/stream-json/wiki/Stringer):
`Stringer` is a [Transform](https://nodejs.org/api/stream.html#stream_class_stream_transform) stream. It consumes a token stream and converts it to text representing a JSON object. It is very useful when you want to edit a stream with filters and a custom code, and save it back to a file.

### []()
Why x ? There are a number [JSON Schema validators](https://json-schema.org/implementations.html#validator-javascript) for node / javascript.
