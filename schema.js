// This is the schema that the JSON schema validator will use. 
// It's based on the $jsonSchema defined in the validation rules for the books collection in the bookdb database.
// I've commented out / replaced items below to convert from Mongo schema validator to JSON schema validator

const schema = 
// {
//   $jsonSchema: 
    {
      type: 'object',
      required: [
        'created'
      ],
      properties: {
        created: {
          type: 'object',
          // format: 'date-time',
          description: 'must be a $date object and is required',
          properties: {
            "$date": {
              type: 'number',
              // format: 'date',
              description: 'must be a $date object and is required'
            }
          }
        }
      }
    }
// }
module.exports.schema = schema
// {
//   $jsonSchema: {
//     bsonType: 'object',
//     required: [
//       'created',
//     ],
//     properties: {
//       created: {
//         bsonType: 'date',
//         description: 'must be a string and is required'
//       },
//     }
//   }
// }