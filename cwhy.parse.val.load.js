// This extends books.parse.validate.js by adding in the bulk write 
// of documents to the mongo db.
// Bulk writes are necessary for performance as mongo will handle 
// the buffering and parallelism of the writes.

const chalk = require('chalk')
const assert = require('assert')

// prepare for mongo connection
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'test'
const client = new MongoClient(url, { useUnifiedTopology: true })

// set up of stream-json 
const {chain}  = require('stream-chain')
const {parser} = require('stream-json')
const {streamValues} = require('stream-json/streamers/StreamValues')
const {streamArray} = require('stream-json/streamers/StreamArray')
const fs       = require('fs')

// set up schema and validator
const Ajv = require("ajv").default
const ajv = new Ajv({}) // options can be passed, e.g. {allErrors: true}
const books = require("./cwhy.schema.js")
const validate = ajv.compile(books.schema)

function start() {
  startTime = new Date()
  console.log(chalk.black.bgWhiteBright(`Start: ${startTime}`))
}

function end() {
  endTime = new Date()
  var timeDiff = endTime - startTime //in ms
  timeDiff /= 1000

  var seconds = Math.round(timeDiff)
  var minutes = Math.round(seconds / 60, 2)
  console.log(chalk.black.bgWhiteBright(`End: ${endTime} duration ${minutes} mins `))
}

start()
client.connect( async (err, client) => {
  
  assert.strictEqual(null, err)
  console.log(chalk.blue('Connected successfully to server'))

  const db = client.db(dbName)
  console.log(chalk.blue(`Switched to ${db.databaseName} database`))
  const cwhy = db.collection('coronawhy')
  let bulk = cwhy.initializeUnorderedBulkOp()

  // define the streaming pipeline
  const pipeline = chain([
    fs.createReadStream('v6_text.json'),
    // parser({jsonStreaming: true}),
    parser({}),
    // streamValues(),
    streamArray(),
    async data => {
      const value = data.value

      // validate streamed JSON object value
      const valid = validate(value)
      if (valid)  {
        // ++accepted
        if (writes>=100000) {
          // console.log("bulkwrite")
          bulk.execute()
          .then(res => {
            cummWrites = cummWrites + writes
            // if (cummWrites>=1000000) {
            //   end()
            //   ++m
            //   console.log(m, "million") 
            //   cummWrites = 0
            // }  
          })
          .catch(err => {
            cummWrites = cummWrites + writes
            console.log(chalk.black.bgYellow(err))
            // if (cummWrites>=1000000) {
            //   end()
            //   ++m
            //   console.log(m, "million") 
            //   cummWrites = 0
            // } 
          })

          bulk = cwhy.initializeUnorderedBulkOp()
          writes = 0
          // console.log("bulkwrite")
          // end()
        //   bulk.execute()
        //     .then(res => {
        //       console.log(`inserted: ${res.nInserted}`)
        //       cummWrites = cummWrites + writes
        //       console.log(`writes: ${cummWrites}`)
        //       end()
        //     })
        //     .catch(err => {
        //       cummWrites = cummWrites + writes
        //       console.log(`writes: ${cummWrites}`)
        //       end()
        //       // console.log(chalk.black.bgYellow(err))
        //     })

        //   writes = 0
        }
        
        bulk.insert( value )
        ++writes
        // ++cummWrites
        return value  
      } else {
        // console.log("fail")
        ++rejected
        // console.log(value._id)
        // console.log(validate.errors)
        return null
      }
    }
  ])

  let rejected = 0
  let accepted = 0
  let writes = 0
  let cummWrites = 0
  let m = 0

  pipeline.on('data', () => ++accepted)
  pipeline.on('end', async () => {
    console.log(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`)
    let res = await bulk.execute().catch(err => {
      console.log(chalk.black.bgYellow(err))
      return {nInserted: 0}
    })
    end()
    // console.log(`inserted: ${res.nInserted}`)
    client.close()
    console.log(chalk.blue('Connection closed'))
  })
    
})