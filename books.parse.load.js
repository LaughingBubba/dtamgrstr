// This extends books.parse.validate.js by adding in the bulk write 
// of documents to the mongo db.
// Bulk writes are necessary for performance as mongo will handle 
// the buffering and parallelism of the writes.

const chalk = require('chalk')
const assert = require('assert')

// prepare for mongo connection
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'bookdb'
const client = new MongoClient(url, { useUnifiedTopology: true })

// set up of stream-json 
const {chain}  = require('stream-chain')
const {parser} = require('stream-json')
const {streamValues} = require('stream-json/streamers/StreamValues')
const fs       = require('fs')

// set up schema and validator
const Ajv = require("ajv").default
const ajv = new Ajv({}) // options can be passed, e.g. {allErrors: true}
const books = require("./books.schema.js")
const validate = ajv.compile(books.schema)

  
client.connect( async (err, client) => {
    
  assert.strictEqual(null, err)
  console.log(chalk.blue('Connected successfully to server'))

  const db = client.db(dbName)
  console.log(chalk.blue(`Switched to ${db.databaseName} database`))
  const col = db.collection('books')
  let bulk = col.initializeUnorderedBulkOp()

  // define the streaming pipeline
  const pipeline = chain([
    fs.createReadStream('books.json'),
    parser({jsonStreaming: true}),
    streamValues(),
    data => {
      const value = data.value

      // validate streamed JSON object value
      const valid = validate(value)
      if (valid)  {
        bulk.insert( value )
        return value  
      } else {
        ++rejected
        console.log(value._id)
        // console.log(validate.errors)
        return null
      }
    }
  ])

  let rejected = 0
  let accepted = 0

  pipeline.on('data', () => ++accepted)
  pipeline.on('end', async () => {
    console.log(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`)
    let res = await bulk.execute().catch(err => {
      console.log(chalk.black.bgYellow(err))
      return {nInserted: 0}
    })
    console.log(`inserted: ${res.nInserted}`)
    client.close()
    console.log(chalk.blue('Connection closed'))
  })
    
})