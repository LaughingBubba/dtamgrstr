const { flatten, inflate } = require('flattenjs')
const chalk = require('chalk')
const assert = require('assert')

const obj =   
{ "_id": 
  {
    "id" : "1",
    "effDate" : {
      "$date": "2020-03-02T01:11:18.965Z"
    }
  },
  "name": "Jack",
  "added": {"$timestamp": {"t":1589821418,"i":0}}
}

const san = { _id: { id: '1', effDate: new Date("2020-03-02T01:11:18.965Z") }, name: 'Jack' }

// prepare for mongo connection
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'test'
const client = new MongoClient(url, { useUnifiedTopology: true })

client.connect( async (err, client) => {
  
  assert.strictEqual(null, err)
  console.log(chalk.blue('Connected successfully to server'))
  const db = client.db(dbName)
  console.log(chalk.blue(`Switched to ${db.databaseName} database`))
  const rhs_col = db.collection('hobbies')

  const qry = await sanitise(obj)

  let rhs = await rhs_col.findOne(qry)

  console.log('qry>',qry)
  console.log('rhs>',rhs)

  // rhs = await rhs_col.findOne(san)

  // console.log('san>',san)
  // console.log('rhs>',rhs)
  
  // rhs = await rhs_col.findOne({_id: { id: '1', effDate: new Date("2020-03-02T01:11:18.965Z") }})

  // console.log('nat>',{_id: { id: '1', effDate: new Date("2020-03-02T01:11:18.965Z") }})
  // console.log('rhs>',rhs)

  client.close()
})



async function sanitise (obj) {
  const mongoJSONDate = '.$date'

  let flatObj = flatten(obj)
  let newObj = {}
  const keys = Object.keys(flatObj)
  await keys.forEach((key, index) => {
    // console.log(caseID, side, key, flatObj[key], typeof flatObj[key])
    if (key.endsWith(mongoJSONDate)) {
      newObj[key.replace(mongoJSONDate,'')] = new Date(flatObj[key])
    } else {
      newObj[key] = flatObj[key] 
    }
    
  }) 
  let sanitised = await inflate(newObj)
  // console.log(sanitised)
  return sanitised
}