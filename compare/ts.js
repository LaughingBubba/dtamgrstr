const { flatten, inflate } = require('flattenjs')
const chalk = require('chalk')
const assert = require('assert')
const Mongo = require('mongodb')

const mongoJSONDate = '.$date'
const mongoJSONDecimal = '.$numberDecimal'
const mongoJSONtimestamp = '.$timestamp.t'
const mongoJSONtimestampI = '.$timestamp.i'

const bsonType = '._bsontype'
const bsonTSHigh = '.high_'
const bsonTSLow = '.low_'
const bsonBytes = 'bytes'

const obj =   
{ "_id": 
  {
    "id" : "1",
    "effDate" : {
      "$date": "2020-03-02T01:11:18.965Z"
    }
  },
  "name": "Jack",
  "added": {"$timestamp": {"t":1589821418,"i":0}}
}

// main()
// async function main() {
//   const qry = await sanitise(obj)JSONtimestampI
// prepare for mongo connection

const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'test'
const client = new MongoClient(url, { useUnifiedTopology: true })

let tsArray = []

client.connect( async (err, client) => {
  
  assert.strictEqual(null, err)
  console.log(chalk.blue('Connected successfully to server'))
  const db = client.db(dbName)
  console.log(chalk.blue(`Switched to ${db.databaseName} database`))
  const rhs_col = db.collection('hobbies')

  const qry = await sanitiseLHS(obj)
  const pk = {_id: qry._id}

  let rhs = await rhs_col.findOne(pk)

  console.log('qry>',qry)
  console.log('rhs>',rhs)
  // console.log('rhsTimestamp>',Mongo.Timestamp(rhs.added).getHighBits())
  console.log('rhsTimestamp>', rhs.added.getHighBits() )
  console.log('san_rhs>', await sanitiseRHS(rhs))

  // rhs = await rhs_col.findOne(san)

  // console.log('san>',san)
  // console.log('rhs>',rhs)
  
  // rhs = await rhs_col.findOne({_id: { id: '1', effDate: new Date("2020-03-02T01:11:18.965Z") }})

  // console.log('nat>',{_id: { id: '1', effDate: new Date("2020-03-02T01:11:18.965Z") }})
  // console.log('rhs>',rhs)

  client.close()
})

async function sanitiseRHS (obj) {
  console.log('tsArray',tsArray)
  let flatObj = flatten(obj)
  // RHS = Mongo doc
  // ATM only sanitises timestamps ATM
  let newObj = {}
  const keys = Object.keys(flatObj)
  await keys.forEach((key, index) => {
    // console.log('flatRHS key',key)
    // if (tsArray.indexOf(key) > -1) {
    // console.log('Timestamp',flatObj[key].getHighBits())
    // newObj[key] = flatObj[key].getHighBits()
    if (key.endsWith(bsonType)) {
      // Ignore
    } else if (key.endsWith(bsonTSLow)) { 
      // Ignore
    } else if (key.endsWith(bsonTSHigh)) { 
      newObj[key.replace(bsonTSHigh,'')] = flatObj[key]
    } else if (key.endsWith(bsonBytes)) {
      console.log('bytes',flatObj[key])
      // ignore incrment and assume = 0 
    } else {
      newObj[key] = flatObj[key] 
    }
  })
  // ATM only sanitises timestamps ATM
  let sanitised = await inflate(newObj)
  return sanitised
}

async function sanitiseLHS (obj) {

  let flatObj = flatten(obj)
  console.log('flat>',flatObj)
  let newObj = {}
  const keys = Object.keys(flatObj)
  await keys.forEach((key, index) => {
    // console.log(caseID, side, key, flatObj[key], typeof flatObj[key])
    if (key.endsWith(mongoJSONDate)) {
      newObj[key.replace(mongoJSONDate,'')] = new Date(flatObj[key])
    } 
    else if (key.endsWith(mongoJSONDecimal)) {
      newObj[key.replace(mongoJSONDecimal,'')] = new Date(flatObj[key])
    } 
    else if (key.endsWith(mongoJSONtimestampI)) {
      // ignore incrment and assume = 0 
    }
    else if (key.endsWith(mongoJSONtimestamp)) {
      console.log('key',key,' var',flatObj[key])
      let wrkKey = key.split('.').slice(0, -2).join('.')
      newObj[key.replace(mongoJSONtimestamp,'')] = flatObj[key]
      // console.log('toInt>',Mongo.Timestamp(0, flatObj[key]).getHighBits())
      tsArray.push(key.replace(mongoJSONtimestamp,''))
    } else {
      newObj[key] = flatObj[key] 
    }
    
  }) 
  let sanitised = await inflate(newObj)
  // console.log(sanitised)
  return sanitised
}