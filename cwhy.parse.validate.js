// This combines books.parse.js and book.validate.js to ensure that
// things play together nicely

// set up of stream-json 
const {chain}  = require('stream-chain')
const {parser} = require('stream-json')
const {streamValues} = require('stream-json/streamers/StreamValues')
const fs       = require('fs')

// set up schema and validator
const Ajv = require("ajv").default
const ajv = new Ajv({}) // options can be passed, e.g. {allErrors: true}
const books = require("./cwhy.schema.js")
const validate = ajv.compile(books.schema)

// define the streaming pipeline
const pipeline = chain([
  fs.createReadStream('cwhy.json'),
  parser({jsonStreaming: true}),
  streamValues(),
  data => {
    const value = data.value

    // validate streamed JSON object value
    const valid = validate(value)
    if (valid)  {
      return value  
    } else {
      ++rejected
      console.log(value.paper_id)
      console.log(validate.errors)
      return null
    }
  }
])

let rejected = 0
let accepted = 0

pipeline.on('data', () => ++accepted)
pipeline.on('end', () =>
  console.log(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`))
