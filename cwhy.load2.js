const chalk = require('chalk')
const assert = require('assert')
const MongoClient = require('mongodb').MongoClient

// set up schema and validator
const Ajv = require('ajv').default
const ajv = new Ajv({}) // options can be passed, e.g. {allErrors: true}
const collection = require('./cwhy.schema.js')
const validate = ajv.compile(collection.schema)

async function run(mongoURI) {
  // prepare for mongo connection
  const client = new MongoClient(mongoURI, { useUnifiedTopology: true })

  return new Promise((resolve, reject) => {
    client.connect( async (err, client) => {
  
      assert.strictEqual(null, err) // Asserts connection OK
      const db = client.db('test')
    
      const cwhy = db.collection('coronawhy')
      let bulk_acc = cwhy.initializeUnorderedBulkOp()
      let accepted = 0
      let accepted_writes = 0

      const cwhy_errs = db.collection('coronawhy_errors')
      let bulk_err = cwhy_errs.initializeUnorderedBulkOp()
      let rejected = 0
      let rejected_writes = 0

      const cursor = db.collection('staging').find({}).stream()
    
      cursor.on('data', async (data) => {

        const valid = validate(data)
        if (valid)  { // PASS Validation
          ++accepted

          await bulk_acc.find({ _id: data_id }).upsert().replaceOne(data)
          ++accepted_writes
          if (accepted_writes === 10000) { 
            await bulk_acc.execute().catch(err => {
              console.log(chalk.black.bgYellow(err))
            })
            bulk_acc = cwhy.initializeUnorderedBulkOp()
            accepted_writes = 0
            validate = ajv.compile(collection.schema) // recreate validator to avoid avj memory leak
          }
        } else { // FAIL Validation
          ++rejected

          await bulk_err.find({ _id: data_id }).upsert().replaceOne(data)
          ++rejected_writes
          if (rejected_writes === 10000) { 
            await bulk_err.execute().catch(err => {
              console.log(chalk.black.bgYellow(err))
            })
            bulk_err = cwhy_errs.initializeUnorderedBulkOp()
            rejected_writes = 0
            validate = ajv.compile(collection.schema) // recreate validator to avoid avj memory leak
          }
        }
      })

      cursor.on('end', async () => {
        if (accepted_writes > 0) {
          await bulk_acc.execute().catch(err => {
            console.log(chalk.black.bgYellow(err))
          })
        }
        if (rejected_writes > 0) {
          await bulk_err.execute().catch(err => {
            console.log(chalk.black.bgYellow(err))
          })
        }
        console.log(chalk.blueBright(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`))
        client.close()
        resolve()
      })   
    })
  })
}

module.exports.run = run