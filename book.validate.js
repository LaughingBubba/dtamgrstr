// This is a basic validation of a book document
// The intention to have understand the basic set up for validation
const Ajv = require("ajv").default
const ajv = new Ajv({allErrors: true}) // options can be passed, e.g. {allErrors: true}
const books = require("./books.schema.js")
const validate = ajv.compile(books.schema)

const valid = validate({_id:1, title: 'something', isbn:"123", status:"NEW", pageCount:1})
if (!valid) console.log(validate.errors)