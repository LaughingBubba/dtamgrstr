// https://attacomsian.com/blog/javascript-iterate-objects
const chalk = require('chalk')
const { flatten } = require('flattenjs')
const sqlite3 = require('sqlite3').verbose()

// const lhs = require('../test/data/base.json')
// const rhs = require('../test/data/test.01.json') // equality
const lhs = require('../test/data/base.simple.json')
const rhs = require('../test/data/test.01.simple.json') // equality

// const rhs = require('../test/data/test.02.json') // functionally the same but different order
// const rhs = require('../test/data/test.03.json') // different days
// const rhs = require('../test/data/test.04.json') // orphan LHS
// const rhs = require('../test/data/test.05.json') // orphan RHS

main()

async function main() {
  const file = 'jtongo.sqlite'
  const db = new sqlite3.Database(file)
  await db.serialize(async () => {
    db.run(`CREATE TABLE IF NOT EXISTS results 
            (lID INT, ls TEXT, lk TEXT, lv TEXT, lt TEXT, 
             rID INT, rs TEXT, rk TEXT, rv TEXT, rt TEXT,
             result TEXT)`)

    db.run('DROP INDEX IF EXISTS co_idx')
    db.run('DROP TABLE IF EXISTS caseOutput')
    db.run('CREATE TABLE IF NOT EXISTS caseOutput (caseID INT, side TEXT, key TEXT, val TEXT, type TEXT)')
    db.run('CREATE INDEX IF NOT EXISTS co_idx ON caseOutput(caseID,side,key)')
  })

  await putXHS(flatten(lhs), '001', 'LHS', db)
  await putXHS(flatten(rhs), '001', 'RHS', db)

  await db.serialize(async () => {
    const tmm = chalk.yellow('TYPE MISMATCH')
    const vmm = chalk.yellow('VALUE MISMATCH')
    const lho = chalk.yellow('LH ORPHAN')
    const rho = chalk.yellow('RH ORPHAN')
    const ok = chalk.green('OK')
    // var xx = "MISMATCH"
    db.run(`DELETE FROM results WHERE (lID = ${1} or lID IS NULL) or (rID = ${1} or rID IS NULL)`)

    // Compare LHS+RHS
    db.run(
      `INSERT INTO results
       SELECT 
        l.caseID, l.side as ls, l.key as lk, l.val as lv, l.type as lt, 
        r.caseID, r.side as rs, r.key as rk, r.val as rv, r.type as rt,
        "" as result
       FROM caseOutput l  
       LEFT
       JOIN caseOutput r 
        ON r.side='RHS' and l.caseID=r.caseID and l.key=r.key
       WHERE l.side='LHS' 
       UNION ALL
       SELECT 
        r.caseID, r.side as ls, r.key as lk, r.val as lv, r.type as lt, 
        l.caseID, l.side as rs, l.key as rk, l.val as rv, l.type as rt,
                  "" as result
       FROM caseOutput l
       LEFT 
       JOIN caseOutput r 
        ON r.side='LHS' and l.caseID=r.caseID and l.key=r.key
       WHERE l.side is 'RHS' and r.side IS NULL
      `
    )

    // Type Mismatch
    db.run(
      `UPDATE results
       SET result = "${tmm}"
       WHERE lt <> rt and result = ""
      `
    )

    // Value Mismatch
    db.run(
      `UPDATE results
       SET result = "${vmm}"
       WHERE lv <> rv and result = ""
      `
    )

    // RH Orphan
    db.run(
      `UPDATE results
       SET result = "${rho}"
       WHERE ls IS NULL
      `
    )

    // LH Orphan
    db.run(
      `UPDATE results
       SET result = "${lho}"
       WHERE rs IS NULL
      `
    )    
    
    // Default OK
    db.run(
      `UPDATE results
       SET result = "${ok}"
       WHERE result = ""
      `
    )

    db.each(
      `SELECT lID, ls, lk, lv, lt, 
              rID, rs, rk, rv, rt, 
              result
       FROM results  
      `, function(err, row) {
      if (err) console.log("Read error: " + err.message)
      // else console.log(row)
      // else console.table(row)
      // else console.log(row.caseID, row.side, row.key, row.val, row.type)
      else console.log(row.lID, row.ls, row.lk, row.lv, row.lt, row.rID, row.rs, row.rk, row.rv, row.rt, row.result )
    })  
  })

  db.close()

}

async function putXHS (flatObj, caseID, side, db) {
  var stmt = db.prepare('INSERT INTO caseOutput VALUES (?, ?, ?, ?, ?)')

  const keys = Object.keys(flatObj)
  await keys.forEach((key, index) => {
    // console.log(caseID, side, key, flatObj[key], typeof flatObj[key])
    stmt.run( caseID, side, key, flatObj[key], typeof flatObj[key] )
  })  

  stmt.finalize()
}