// This is a simple parsing of the books.json file
// Although this file is small, the intention is that this process
// should be able handle JSON file sizes in the order of multiple GB's
// Hence the use of a file stream and then parsing the stream (like an XML SAX parser)
// As the focus is getting the stream/parsing to work, it only simulates the use of 
// a validator by testing for the existance of the pageCount attribute and > zero
// so that accepted and rejected counts can be tallied

const {chain}  = require('stream-chain')
 
const {parser} = require('stream-json')
const {streamValues} = require('stream-json/streamers/StreamValues')
 
const fs   = require('fs')

const pipeline = chain([
  fs.createReadStream('books.json'),
  parser({jsonStreaming: true}),
  streamValues(),
  data => {
    const value = data.value

    if (value.pageCount && value.pageCount > 0) {
      return value  
    } else {
      ++rejected
      console.log(value._id)
      return null
    }
  }
])
 
let rejected = 0
let accepted = 0
pipeline.on('data', () => ++accepted);
pipeline.on('end', () =>
  console.log(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`));