// LHS JSON Parse and Compare (with Mongo)
// This combines books.parse.js and book.validate.js to ensure that
// things play together nicely
const { printTable } = require('console-table-printer')
const chalk = require('chalk')
const assert = require('assert')
const { flatten, inflate } = require('flattenjs')
const sqlite3 = require('sqlite3').verbose()

const mongoJSONDate = '.$date'
const mongoJSONDecimal = '.$numberDecimal'
const mongoJSONtimestamp = '.$timestamp.t'
const mongoJSONtimestampI = '.$timestamp.i'

const bsonType = '._bsontype'
const bsonTSHigh = '.high_'
const bsonTSLow = '.low_'
const bsonBytes = 'bytes'

// prepare for mongo connection
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'test'
const client = new MongoClient(url, { useUnifiedTopology: true })

// set up of stream-json 
// const lhs = require('../test/data/base.2.mongo.json')
const lhs = '../test/data/base.2.mongo.compound.deep.json'
const {chain}  = require('stream-chain')
const {parser} = require('stream-json')
const {streamArray} = require('stream-json/streamers/StreamArray')
const fs       = require('fs')

let tsArray = []
let ndArray = []

client.connect( async (err, client) => {
  
  assert.strictEqual(null, err)
  console.log(chalk.blue('Connected successfully to server'))

  // set up sqlite3 tables
  const file = 'jtongo.sqlite'
  const SQLdb = new sqlite3.Database(file)
  await SQLdb.serialize(async () => {
    SQLdb.run(`CREATE TABLE IF NOT EXISTS results 
            (lID INT, ls TEXT, li TEXT, lk TEXT, lv TEXT, lt TEXT, 
             rID INT, rs TEXT, ri TEXT, rk TEXT, rv TEXT, rt TEXT,
             result TEXT)`)

    SQLdb.run('DROP INDEX IF EXISTS co_idx')
    SQLdb.run('DROP TABLE IF EXISTS caseOutput')
    SQLdb.run(`CREATE TABLE IF NOT EXISTS caseOutput 
              (caseID INT, side TEXT, _id TEXT, key TEXT, val TEXT, type TEXT)`)
    SQLdb.run(`CREATE INDEX IF NOT EXISTS co_idx 
               ON caseOutput(caseID,side,_id,key)`)
  })

  const db = client.db(dbName)
  console.log(chalk.blue(`Switched to ${db.databaseName} database`))
  const rhs_col = db.collection('hobbies')

  // define the streaming pipeline
  const pipeline = chain([
    fs.createReadStream(lhs),
    // parser({jsonStreaming: true}),
    parser({}),
    streamArray(),
    async data => {
      const lhs = data.value
      const san_lhs = await sanitise(lhs)
      const pk = {_id: san_lhs._id}
      console.log('pk>',pk)

      let rhs = await rhs_col.findOne(pk)
      // console.log('rhs>',rhs)
      // console.log('san_lhs>',san_lhs)

      if (rhs == null) rhs = {"_id":{}}

      let san_rhs = await sanitiseRHS(rhs)
      // console.log('san_rhs>',san_rhs)

      await putXHS(flatten(san_lhs), '001', 'LHS', san_lhs._id, SQLdb)
      await putXHS(flatten(san_rhs), '001', 'RHS', rhs._id, SQLdb)

      // console.log(rhs)
      // console.log(chalk.greenBright(JSON.stringify(rhs,null,2)))

      return
    }
  ])

  pipeline.on('data', () => console.log(chalk.blueBright('doc')))
  pipeline.on('end', async () => {
    await compare(SQLdb)
    SQLdb.close()

    client.close()
    
    console.log(chalk.blue('Connections closed'))
    console.log(chalk.blueBright(`END`))
  })   

})

async function sanitise (obj) {
  tsArray = []
  ndArray = []
  let flatObj = flatten(obj)
  let newObj = {}
  const keys = Object.keys(flatObj)
  await keys.forEach((key, index) => {
    // console.log(caseID, side, key, flatObj[key], typeof flatObj[key])
    console.log(key, ':', flatObj[key], typeof flatObj[key])

    if (key.endsWith(mongoJSONDate)) {
      newObj[key.replace(mongoJSONDate,'')] = new Date(flatObj[key])
    }
    else if (key.endsWith(mongoJSONDecimal)) {
      // newObj[key.replace(mongoJSONDecimal,'')] = Number.parseFloat(flatObj[key])
      newObj[key.replace(mongoJSONDecimal,'')] = flatObj[key]
      ndArray.push(key.replace(mongoJSONDecimal,''))
    }
    else if (key.endsWith(mongoJSONtimestampI)) {
      // ignore incrment and assume = 0 
    }
    else if (key.endsWith(mongoJSONtimestamp)) {
      // console.log('key',key,' var',flatObj[key])
      // let wrkKey = key.split('.').slice(0, -2).join('.')
      newObj[key.replace(mongoJSONtimestamp,'')] = flatObj[key]
      // console.log('toInt>',Mongo.Timestamp(0, flatObj[key]).getHighBits())
      tsArray.push(key.replace(mongoJSONtimestamp,''))
    } else {
      newObj[key] = flatObj[key] 
    }
    
  }) 
  console.log('newObj>',newObj)
  let sanitised = await inflate(newObj)
  console.log('inflated>',sanitised)
  let flatAgain = await flatten(sanitised)
  console.log('flatAgain>',flatAgain)
  return sanitised
}

async function sanitiseRHS (obj) {
  obj[ndArray[0]] = obj[ndArray[0]].toString()
  // console.log(obj)
  // console.log('obj.rating',obj.rating.toString(), typeof obj.rating.toString())
  let flatObj = flatten(obj)
  // RHS = Mongo doc
  // ATM only sanitises timestamps ATM
  let newObj = {}
  const keys = Object.keys(flatObj)
  await keys.forEach((key, index) => {
    // console.log('flatRHS key',key)
    // if (tsArray.indexOf(key) > -1) {
    // console.log('Timestamp',flatObj[key].getHighBits())
    // newObj[key] = flatObj[key].getHighBits()
    if (key.endsWith(bsonType)) {
      // Ignore
    } else if (key.endsWith(bsonTSLow)) { 
      // Ignore
    } else if (key.endsWith(bsonTSHigh)) { 
      newObj[key.replace(bsonTSHigh,'')] = flatObj[key]    
    } else if (key.endsWith(bsonBytes)) {
      console.log('bytes<<<<>>>>>>>>>>',flatObj[key])
      // ignore incrment and assume = 0 
    } else {
      newObj[key] = flatObj[key] 
    }
  })
  // ATM only sanitises timestamps ATM
  let sanitised = await inflate(newObj)
  return sanitised
}

async function putXHS (flatObj, caseID, side, _id, db) {
  var stmt = db.prepare('INSERT INTO caseOutput VALUES (?, ?, ?, ?, ?, ?)')
  var flatID = await JSON.stringify(_id)
  const keys = Object.keys(flatObj)
  await keys.forEach((key, index) => {
    // console.log(caseID, side, key, flatObj[key], typeof flatObj[key])
    stmt.run( caseID, side, flatID, key, flatObj[key], typeof flatObj[key] )
  })  

  stmt.finalize()
  return
}

async function compare (db) {
  await db.serialize(async () => {
    const tmm = chalk.yellow('TYPE MISMATCH')
    const vmm = chalk.yellow('VALUE MISMATCH')
    const lho = chalk.yellow('LH ORPHAN')
    const rho = chalk.yellow('RH ORPHAN')
    const ok = chalk.green('OK')
    var report = []

    db.run(`DELETE FROM results WHERE (lID = ${1} or lID IS NULL) or (rID = ${1} or rID IS NULL)`)

    // Compare LHS+RHS
    db.run(
      `INSERT INTO results
       SELECT 
        l.caseID, l.side as ls, l._id as li, l.key as lk, l.val as lv, l.type as lt, 
        r.caseID, r.side as rs, r._id as ri, r.key as rk, r.val as rv, r.type as rt,
        "" as result
       FROM caseOutput l  
       LEFT
       JOIN caseOutput r 
        ON r.side='RHS' and l.caseID=r.caseID and l._id=r._id and l.key=r.key
       WHERE l.side='LHS' 
       UNION ALL
       SELECT 
        l.caseID, r.side as rs, l._id as ri, r.key as lk, r.val as lv, r.type as lt, 
        r.caseID, l.side as ls, r._id as li, l.key as rk, l.val as rv, l.type as rt,
                  "" as result       
       FROM caseOutput l
       LEFT 
       JOIN caseOutput r 
        ON r.side='LHS' and l.caseID=r.caseID  and l._id=r._id and l.key=r.key
       WHERE l.side is 'RHS' and r.side IS NULL
      `
    )

    // Type Mismatch
    db.run(
      `UPDATE results
       SET result = "${tmm}"
       WHERE lt <> rt and result = ""
      `
    )

    // Value Mismatch
    db.run(
      `UPDATE results
       SET result = "${vmm}"
       WHERE lv <> rv and result = ""
      `
    )

    // RH Orphan
    db.run(
      `UPDATE results
       SET result = "${rho}"
       WHERE ls IS NULL
      `
    )

    // LH Orphan
    db.run(
      `UPDATE results
       SET result = "${lho}"
       WHERE rs IS NULL
      `
    )    
    
    // Default OK
    db.run(
      `UPDATE results
       SET result = "${ok}"
       WHERE result = ""
      `
    )

    db.all(
      `SELECT lID as CaseID, li as "Primakry Key", lk as "lh Property", lv as "lh Value", lt as "lh Type", 
              result,                              rk as "rh Property", rv as "rh Value", rt as "rh Type"  
              
       FROM results  
       ORDER BY COALESCE(lID, rID), COALESCE(li, ri)
      `, function(err, rows) {
      if (err) console.log("Read error: " + err.message)
      else {
        // rows.forEach(row => {
        //   console.log(row.lID, row.ls, row.li, row.lk, row.lv, row.lt, row.rID, row.rs, row.li, row.rk, row.rv, row.rt, row.result )
        // })
        printTable(rows)
      }
    })
   
  })
}