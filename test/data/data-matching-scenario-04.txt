Scenario: Compare same documents with additional document in the source
Given a "JSON file" with the following documents with the primary key(s) "pk1":
{
  "pk1": "1",
  "name": "Jack",
  "hobbyArray": [
    {
      "hobbyCode": "CHS",
      "hobbyName": "Chess",
      "daysAWeek": "3"
    },
    {
      "hobbyCode": "CRK",
      "hobbyName": "Cricket",
      "daysAWeek": "1"
    }
  ]
},
{
  "pk1": "2",
  "name": "Diane"
  "hobbyArray": [
    {
      "hobbyCode": "CHS",
      "hobbyName": "Chess",
      "daysAWeek": "1"
    },
    {
      "hobbyCode": "REA",
      "hobbyName": "Reading",
      "daysAWeek": "5"
    },
    {
      "hobbyCode": "GUI",
      "hobbyName": "Guitar",
      "daysAWeek": "3"
    }
  ]
}
And a "MongoDB collection" with the following documents with the primary key(s) "pk1":
{
  "pk1": "1",
  "name": "Jack",
  "hobbyArray": [
    {
      "hobbyCode": "CHS",
      "hobbyName": "Chess",
      "daysAWeek": "3"
    },
    {
      "hobbyCode": "CRK",
      "hobbyName": "Cricket",
      "daysAWeek": "1"
    }
  ]
}
When IDTW comparison runs:
Then the IDTW result is:
| Sl No. | Source                  | Source Value | Target                  | Target Value | Result        |
| 1      | pk1                     | 1            | pk1                     | 1            | Match         |
| 2      | name                    | Jack         | name                    | Jack         | Match         |
| 3      | hobbyArray[0].hobbyCode | CHS          | hobbyArray[0].hobbyCode | CHS          | Match         |
| 4      | hobbyArray[0].hobbyName | Chess        | hobbyArray[0].hobbyName | Chess        | Match         |
| 5      | hobbyArray[0].daysAWeek | 3            | hobbyArray[0].daysAWeek | 3            | Match         |
| 6      | hobbyArray[1].hobbyCode | CRK          | hobbyArray[1].hobbyCode | CRK          | Match         |
| 7      | hobbyArray[1].hobbyName | Cricket      | hobbyArray[1].hobbyName | Cricket      | Match         |
| 8      | hobbyArray[1].daysAWeek | 1            | hobbyArray[1].daysAWeek | 1            | Match         |
| --     | End                     | of           | document                | --           | --            |
| 1      | pk1                     | 2            | --                      | --           | SOURCE ORPHAN |
| 2      | name                    | Diane        | --                      | --           | SOURCE ORPHAN |
| 3      | hobbyArray[0].hobbyCode | CHS          | --                      | --           | SOURCE ORPHAN |
| 4      | hobbyArray[0].hobbyName | Chess        | --                      | --           | SOURCE ORPHAN |
| 5      | hobbyArray[0].daysAWeek | 1            | --                      | --           | SOURCE ORPHAN |
| 6      | hobbyArray[1].hobbyCode | REA          | --                      | --           | SOURCE ORPHAN |
| 7      | hobbyArray[1].hobbyName | Reading      | --                      | --           | SOURCE ORPHAN |
| 8      | hobbyArray[1].daysAWeek | 5            | --                      | --           | SOURCE ORPHAN |
| 9      | hobbyArray[2].hobbyCode | GUI          | --                      | --           | SOURCE ORPHAN |
| 10     | hobbyArray[2].hobbyName | Guitar       | --                      | --           | SOURCE ORPHAN |
| 11     | hobbyArray[2].daysAWeek | 3            | --                      | --           | SOURCE ORPHAN |
| --     | End                     | of           | document                | --           | --            |

