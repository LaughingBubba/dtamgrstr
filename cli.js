// Reference: https://nodejs.org/en/knowledge/command-line/how-to-parse-command-line-arguments/
// Parameter: MUST be in order:
// 1) Mandatory: JSON path/file
// 2) Mandatory: Mongo URI https://docs.mongodb.com/drivers/node/fundamentals/connection/
// 3) Options: Clear collection (default=Y)

const { promises: fs } = require('fs')
const { MongoClient } = require('mongodb')
const yargs = require('yargs')
const chalk = require('chalk')

const migration = require('./load.js')

const argv = yargs
  .usage('Usage: $0 <command> [options]')
  .demandOption(['json','mongo'])
  .default('cc', 'y')
  .argv

function start() {
  startTime = new Date()
  console.log(chalk.black.bgWhiteBright(`Start: ${startTime}`))
}

function end() {
  endTime = new Date()
  var timeDiff = endTime - startTime //in ms
  timeDiff /= 1000

  var seconds = Math.round(timeDiff)
  var minutes = Math.round(seconds / 60, 2)
  console.log(chalk.black.bgWhiteBright(`End: ${endTime} duration ${minutes} mins `))
}

async function checkMongo(uri) {
  const client = new MongoClient(uri, { useUnifiedTopology: true })
  try {
    // Connect the client to the server
    await client.connect()
    // Establish and verify connection
    await client.db('admin').command({ ping: 1 })
    console.log(chalk.blueBright('Connection to mongoDB OK'))
  } catch (err) {
    throw `Connection failed: ${uri}`
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close()
  }
}

async function checkFile(file) {
  try {
    await fs.access(file)
    console.log(chalk.blueBright('File found:', file))
  } catch (err) {
    throw `File not found: ${file}`
  }
}

async function execShellCommand(cmd) {
  const exec = require('child_process').exec
  return new Promise((resolve, reject) => {
   exec(cmd, (error, stdout, stderr) => {
    if (error) {
     reject(error)
    }
    resolve(stdout? stdout : stderr)
   })
  })
 }

async function main() {
  start()
  let statusOK = true
  try {
    await checkMongo(argv.mongo)
    await checkFile(argv.json)

    console.log(chalk.blueBright('Running mongoimport command'))
    const db = 'test'
    const col = 'staging'
    let command = `mongoimport ${argv.mongo} --authenticationDatabase admin --file ${argv.json} -d ${db} -c ${col} --drop --jsonArray`
    await execShellCommand(command)
    end()
    console.log(chalk.blueBright('Running migration'))
    start()
    await migration.run(argv.mongo)

  } catch (err) {
    statusOK = false
    console.error(chalk.black.bgYellow(err))
  } finally {
    console.log(chalk.blueBright('Run status:'), statusOK ? chalk.greenBright('OK') : chalk.yellow('Fail') )
    end()
  }
}

main()
