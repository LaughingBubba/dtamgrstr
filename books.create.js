// Creates the bookdb and a books collection with validation rules

const chalk = require('chalk')
const MongoClient = require('mongodb').MongoClient
const assert = require('assert')

const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'bookdb'

const client = new MongoClient(url, { useUnifiedTopology: true })


client.connect( async (err, client) => {
   
   assert.strictEqual(null, err)
   console.log(chalk.blue('Connected successfully to server'))

   const db = client.db(dbName)
   console.log(chalk.blue(`Switched to ${db.databaseName} database`))
   
   try { 
      await db.createCollection('books', { 
        capped: false,
        validator: {
          $jsonSchema: 
          {
            bsonType: 'object',
            required: [
              'title',
              'isbn',
              'pageCount',
              'status'
            ],
            properties: {
              title: {
                bsonType: 'string',
                description: 'must be a string and is required'
              },
              isbn: {
                bsonType: 'string',
                description: 'must be a string and is required'
              },
              pageCount: {
                bsonType: 'int',
                minimum: 1,
                maximum: 10000,
                description: 'must be an integer in [ 1, 10000 ] and is required'
              },
              status: {
                'enum': [
                  'PUBLISH',
                  'MEAP',
                  'NEW'
                ],
                description: 'can only be one of the enum values and is required'
              }
            }
          }
        }
      })
      console.log(chalk.greenBright(`Collection created: books`))

   } catch (err) {
      console.log(chalk.black.bgYellow(err))
   }
   
   client.close()
   console.log(chalk.blue('Connection closed'))
   
})