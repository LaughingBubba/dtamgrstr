// This is a simple insert into the collection that is designed to fail.
// You'll see that the only inforamtion you get back is 
// that the document failed validation, but not where or why.
// NB: This also applies to the mongoimport utility

const chalk = require('chalk')
const MongoClient = require('mongodb').MongoClient
const assert = require('assert')

const url = 'mongodb://root:access@localhost:27017/?authSource=admin'
const dbName = 'bookdb'

const client = new MongoClient(url, { useUnifiedTopology: true })


client.connect( async (err, client) => {
   
   assert.strictEqual(null, err)
   console.log(chalk.blue('Connected successfully to server'))

   const db = client.db(dbName)
   console.log(chalk.blue(`Switched to ${db.databaseName} database`))
   
   try { 
      const coll = db.collection('books')

      const res = await coll.insertOne({pageCount:1})
      console.log(chalk.greenBright(`inserted docs: ${res.insertedCount}, id: ${res.insertedId}`))

   } catch (err) {
      console.log(chalk.black.bgYellow(err))
   }
   
   client.close()
   console.log(chalk.blue('Connection closed'))
   
})