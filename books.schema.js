// This is the schema that the JSON schema validator will use. 
// It's based on the $jsonSchema defined in the validation rules for the books collection in the bookdb database.
// I've commented out / replaced items below to convert from Mongo schema validator to JSON schema validator

const schema = 
// {
//   $jsonSchema: 
    {
      // bsonType: 'object',
      type: 'object',
      required: [
        'title',
        'isbn',
        'pageCount',
        'status'
      ],
      properties: {
        title: {
          // bsonType: 'string',
          type: 'string',
          // description: 'must be a string and is required'
        },
        isbn: {
          // bsonType: 'string',
          type: 'string',
          description: 'must be a string and is required'
        },
        pageCount: {
          // bsonType: 'int',
          type: 'integer',
          minimum: 1,
          maximum: 10000,
          description: 'must be an integer in [ 1, 10000 ] and is required'
        },
        status: {
          'enum': [
            'PUBLISH',
            'MEAP',
            'NEW'
          ],
          description: 'can only be one of the enum values and is required'
        }
      }
    }
// }
module.exports.schema = schema