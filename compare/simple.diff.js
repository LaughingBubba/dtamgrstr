// https://attacomsian.com/blog/javascript-iterate-objects
const diff = require('deep-diff').diff
const { flatten } = require('flattenjs')
// const sqlite3 = require('sqlite3').verbose()

const lhs = require('../test/data/base.json')
const rhs = require('../test/data/test.01.json') // equality

// const file = 'jtongo'
// const db = new sqlite3.Database(file)
// const rhs = require('../test/data/test.02.json') // functionally the same but different order
// const rhs = require('../test/data/test.03.json') // different days
// const rhs = require('../test/data/test.04.json') // orphan LHS
// const rhs = require('../test/data/test.05.json') // orphan RHS
// let delta = diff(lhs, rhs)
// console.log('lhs:', flatten(lhs))

const flatLHS = flatten(lhs)
const flatRHS = flatten(rhs)

for (const item in flatLHS) {
  // console.log(item.value)
  if (flatLHS.hasOwnProperty(item)) {
    console.log(`${item}: ${flatLHS[item]}`);
  }
}

const keysLH = Object.keys(flatLHS)
keysLH.forEach((key, index) => {
  console.log(key, flatLHS[key], typeof flatLHS[key])
})


const keysRH = Object.keys(flatRHS)
keysRH.forEach((key, index) => {
  console.log(key, flatRHS[key], typeof flatRHS[key])
})


// const kys = Object.keys(lhs)
// kys.forEach((key, index) => {
//   console.log(key, lhs[key] )
// })
// console.log(delta)

// test 4 & 5 specific
// console.log(`${JSON.stringify(delta,null,2)}`)const file = 'jtongo'
// const db = new sqlite3.Database(file)
