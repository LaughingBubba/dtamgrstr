const chalk = require('chalk')
const assert = require('assert')
const MongoClient = require('mongodb').MongoClient

// set up schema and validator
const Ajv = require('ajv').default
const ajv = new Ajv({}) // options can be passed, e.g. {allErrors: true}
const addFormats = require('ajv-formats')
addFormats(ajv)
const collection = require('./schema.js')
const validate = ajv.compile(collection.schema)

async function run(mongoURI) {
  // prepare for mongo connection
  const client = new MongoClient(mongoURI, { useUnifiedTopology: true })
  const dbName = 'test'

   return new Promise((resolve, reject) => {
    client.connect( async (err, client) => {
  
      // Asserts connection OK
      assert.strictEqual(null, err)
    
      const db = client.db(dbName)
      console.log(chalk.blueBright(`Switched to ${db.databaseName} database`))
      const cwhy = db.collection('coln')
      let bulk = cwhy.initializeUnorderedBulkOp()
    
      let writes = 0
      let rejected = 0
      let accepted = 0
      const cursor = db.collection('staging').find({})
    
      cursor.on('data', async (data) => {
        const valid = validate(data)
        if (valid)  {
          ++accepted
          if (writes >= 100000) {
            bulk.execute()
            .then(res => {return res}
            )
            .catch(err => {
              console.log(chalk.black.bgYellow(err))
            })
            bulk = cwhy.initializeUnorderedBulkOp()
            writes = 0
          }
          bulk.insert( data )
          ++writes
        } else {
          ++rejected
        }
      })
      cursor.on('end', async () => {
        let res = await bulk.execute().catch(err => {
          console.log(chalk.black.bgYellow(err))
          return {nInserted: 0}
        })
        console.log(chalk.blueBright(`${accepted} documents accepted, ${rejected} rejected, total processed: ${accepted+rejected}`))
        client.close()
        resolve()
      })   
    })
  })
}

module.exports.run = run